# Top file
base:
  '*':
    - schedule
  'os:CentOS':
    - match: grain
    - sshkeys
    - software
    - sshd_config
    - firewall
    - users
    - shell_config
  'os:Debian':
    - match: grain
    - sshkeys
    - sshd_config
    - users
    - shell_config

/home/dplaker/.ssh/:
  file.directory:
    - user: dplaker
    - group: dplaker
    - mode: 700

/home/dplaker/.ssh/authorized_keys:
  file.managed:
    - source: salt://files/ssh_keys/dplaker_id_rsa.pub
    - user: dplaker
    - group: dplaker
    - mode: 600
